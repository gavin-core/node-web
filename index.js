'use strict'

const bodyParser = require('body-parser')
const express = require('express')
const cookieParser = require('cookie-parser')
const path = require('path')
const useragent = require('./lib/user-agent')
const _rootPath = path.dirname(require.main.filename)
let _enabledCookieParsing = false

// console.log(_rootPath);

module.exports = {
  sendexpressResponder: require('./lib/sendexpress-responder'),
  useragent: useragent,
  createServer: require('./lib/server'),
  bodyParser: bodyParser,
  express: express,
  viewEngines: {
    ejs: function (app, viewPath, isFullPath) {
      viewPath = viewPath || '/views'
      app.set('view engine', 'ejs')

      if (isFullPath) {
        app.set('views', viewPath)
      } else {
        app.set('views', path.join(_rootPath, viewPath))
      }
    }
  },
  enableCookieParser: (app, secret) => {
    app.use(cookieParser(secret))
  },
  enableStaticFiles: function (app, _path, physicalPath, isFullPath) {
    if (Array.isArray(_path)) {
      for (var i = 0; i < _path.length; i++) { module.exports.enableStaticFiles(app, _path[i][0], _path[i][1], !!_path[i][2]) }

      return
    }

    physicalPath = physicalPath || _path

    if (isFullPath) { app.use(_path, express.static(physicalPath)) } else { app.use(_path, express.static(path.join(_rootPath, physicalPath))) }
  },
  enableRequestTracking: function (app) {
    let reqNum = 0
    app.use((req, res, next) => {
      console.log(++reqNum + ': Request received: ' + req.path + ' (' + req.method + ')')
      next()
    })

    module.exports.enableRequestTracking = () => {}
  },
  enableSendExpressResponder: function (app, servingExternal) {
    app.use(module.exports.sendexpressResponder(servingExternal))
    module.enableSendExpressResponder = function () {}
  },
  enableUserAgent: function (app) {
    app.use(useragent.express())
    app.use(function (req, res, next) {
      req.device = {
        os: req.useragent.OS,
        platform: req.useragent.Platform,
        browser: req.useragent.Browser,
        version: req.useragent.Version,

        isMobile: req.useragent.isMobile,
        isiPhone: req.useragent.isiPhone,
        isiPad: req.useragent.isiPad,
        isAndroid: req.useragent.isAndroid,
        isBlackberry: req.useragent.isBlackberry,
        isWindowsPhone: req.useragent.isWindowsPhone
      }
      next()
    })
  },
  loaders: {
    cookieParser: function (app, cookieName, secret) {
      if (_enabledCookieParsing) {
        return
      }

      _enabledCookieParsing = true

      // exports.express.cookieParser = cookieParser();
      app.use(cookieParser(secret))
      app.use(require('./lib/cookies')(cookieName, true))
    },
    company: function (app) {
      app.use(require('./lib/company-loader')())
    },
    member: function (app) {
      // TODO - this requires that the user loader be used, if not,
      // a member will never be retrieved as a userid wasn't identified
      // might need to force user-loading if member is required, or,
      // throw warning to say user-loader is required
      app.use(require('./lib/member-loader')())
    },
    user: function (app) {
      app.use(require('./lib/user-loader')())
    }
  }
}
