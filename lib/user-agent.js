module.exports = require('express-useragent')
const uaExpress = module.exports.express

module.exports.express = () => {
  const ua = uaExpress()

  return (_req, _res, _next) => {
    const req = _req
    const res = _res
    const next = _next

    ua(req, res, () => {
      req.useragent.isIE6Less = (() => {
        return req.useragent.isIE && req.useragent.Version && +(('' + req.useragent.Version).split('.')[0]) <= 6
      })()

      req.useragent.isIE7 = (() => {
        return req.useragent.isIE && req.useragent.Version && +(('' + req.useragent.Version).split('.')[0]) === 7
      })()
      req.useragent.isIE7Less = (() => {
        return req.useragent.isIE && req.useragent.Version && +(('' + req.useragent.Version).split('.')[0]) <= 7
      })()

      req.useragent.isIE8 = (() => {
        return req.useragent.isIE && req.useragent.Version && +(('' + req.useragent.Version).split('.')[0]) === 8
      })()
      req.useragent.isIE8Less = (() => {
        return req.useragent.isIE && req.useragent.Version && +(('' + req.useragent.Version).split('.')[0]) <= 8
      })()

      req.useragent.isIE9 = (() => {
        return req.useragent.isIE && req.useragent.Version && +(('' + req.useragent.Version).split('.')[0]) === 9
      })()
      req.useragent.isIE9Less = (() => {
        return req.useragent.isIE && req.useragent.Version && +(('' + req.useragent.Version).split('.')[0]) <= 9
      })()

      req.useragent.isIE10 = (() => {
        return req.useragent.isIE && req.useragent.Version && +(('' + req.useragent.Version).split('.')[0]) === 10
      })()
      req.useragent.isIE10Less = (() => {
        return req.useragent.isIE && req.useragent.Version && +(('' + req.useragent.Version).split('.')[0]) <= 10
      })()

      req.useragent.isIE11 = (() => {
        return req.useragent.isIE && req.useragent.Version && +(('' + req.useragent.Version).split('.')[0]) === 11
      })()
      req.useragent.isIE11Less = (() => {
        return req.useragent.isIE && req.useragent.Version && +(('' + req.useragent.Version).split('.')[0]) <= 11
      })()

      next()
    })
  }
}
