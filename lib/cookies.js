const thriftServices = require('node-thrift').thriftServices
const cryptographyService = require('../services/CryptographyService.js')
const cryptographyModels = require('../services/cryptography_types')

module.exports = (cookieName, encrypted) => {
  return (req, res, next) => {
    var cookieValue = req.cookies[cookieName]
    if (!cookieValue || !encrypted) {
      return next()
    }

    thriftServices.getClient('CryptographyService', '0', cryptographyService, (err, client) => {
      if (err) {
        return next(err)
      }

      if (!client) {
        return res.sendexpress.serviceUnavailable(null, 'CryptographyService v0')
      }

      client.decrypt(new cryptographyModels.CryptoModel({ input: cookieValue, method: 'asdf' }), (err, result) => {
        if (err) {
          return next(err)
        }

        if (result) {
          result = JSON.parse(result)
        }

        req.userId = result.value
        next()
      })
    })
  }
}
