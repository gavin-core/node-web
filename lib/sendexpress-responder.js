const _sendexpressExternal = (req, res, _exMappings) => {
  return (err, result) => {
    if (err) {
      if (typeof err === 'string') {
        return res.sendexpress.notFound(err)
      }

      if (err.name) { // Handling an exception thrown from a service
        if (_exMappings && (err.name in _exMappings)) { // This error has predefined results{
          return res.status(_exMappings[err.name].code)
            .send({
              message: _exMappings[err.name].msg || err.message,
              detail: _exMappings[err.name].detail || err.detail
            })
        }

        // convert to generic err structure
        return res.status(500)
          .send({
            message: err.message,
            detail: err.detail
          })
      }

      return res.status(500).send(err)
    }

    if (typeof result === 'undefined' || result === null) {
      return res.sendexpress.notFound()
    }

    // check for boolean results
    if (typeof result === 'boolean' || result.toString() === 'true' || result.toString() === 'false') {
      return res.sendexpress.success(result)
    }

    res.send(result)
  }
}

const _sendexpressInternal = (req, res, _exMappings) => {
  return (err, result) => {
    if (!err) {
      return res.send(result || null)
    }

    if (err.name) { // Handling an exception thrown from a service
      if (_exMappings && (err.name in _exMappings)) { // This error has predefined results
        return res.status(_exMappings[err.name].code)
          .send({
            message: _exMappings[err.name].msg || err.message,
            detail: _exMappings[err.name].detail || err.detail
          })
      }

      // convert to generic err structure
      return res.status(500)
        .send({
          message: err.message,
          detail: err.detail
        })
    }

    res.status(500).send(err)
  }
}

module.exports = servingExternal => {
  const core = require('node-core')
  const _ = core._

  return (req, res, next) => {
    var _exMappings = {
      'ForbiddenException': { code: 403 },
      'PreconditionFailedException': { code: 412 }
    }

    res.sendexpress = servingExternal ? _sendexpressExternal(req, res, _exMappings) : _sendexpressInternal(req, res, _exMappings)
    res.sendexpress.on = function (mappings) {
      if (typeof mappings === 'object') {
        if (!_exMappings) {
          _exMappings = mappings
        } else {
          _.extend(_exMappings, mappings)
        }
      } else {
        _exMappings = _exMappings || {}
        _exMappings[arguments[0]] = { code: arguments[1], msg: arguments[2] }
      }
      return res.sendexpress
    }

    res.sendexpress.success = message => {
      res.status(200).send({ message })
    }
    res.sendexpress.err = (message, detail) => {
      res.status(500).send({ message, detail })
    }
    res.sendexpress.forbidden = message => {
      res.status(403).send({ message })
    }
    res.sendexpress.notFound = message => {
      res.status(404).send({ message })
    }
    res.sendexpress.preconditionFailed = message => {
      res.status(412).send({ message })
    }
    res.sendexpress.expectationFailed = message => {
      res.status(417).send({ message })
    }
    res.sendexpress.notImplemented = message => {
      res.status(501).send({ message: message || 'Not yet implemented' })
    }
    res.sendexpress.serviceUnavailable = (message, serviceName) => {
      res.status(503).send({ message: message || 'A service' + (serviceName ? '(' + serviceName + ')' : '') + ' is currently unavailable, your request cannot be handled at this time' })
    }
    res.sendexpress.temporaryRedirect = url => {
      res.redirect(307, url)
    }
    res.sendexpress.unauthorized = message => {
      res.status(401).send({ message: message || 'You are unauthorized to make this request.' })
    }

    next()
  }
}
