'use strict'

const thriftServices = require('node-thrift').thriftServices
const memberService = require('../services/MemberService')
// const memberServiceModels = require('../services/members_types')

module.exports = () => {
  return (req, res, next) => {
    req.memberId = null
    req.member = null

    if (!req.userId || !req.companyId) {
      return next()
    }

    thriftServices.getClient('MemberService', '0', memberService, (err, client) => {
      if (err) {
        return next(err)
      }

      if (!client) {
        return res.sendexpress.serviceUnavailable(null, 'MemberService v0')
      }

      client.getMemberId(req.userId, req.companyId, (err, memberId) => {
        if (err) {
          return next(err)
        }

        if (!memberId) {
          return next()
        }

        req.memberId = memberId

        client.getContext(req.memberId, (err, context) => {
          if (err) {
            return next(err)
          }

          if (req.userId !== context.userId) {
            req.memberId = null
            return next()
          }

          delete context.userId
          req.member = context
          next()
        })
      })
    })
  }
}
