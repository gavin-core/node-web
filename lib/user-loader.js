
const thriftServices = require('node-thrift').thriftServices
const userService = require('../services/UserService.js')
// const userServiceModels = require('../services/users_types')

module.exports = () => {
  return (req, res, next) => {
    req.user = null
    if (!req.userId) {
      return next()
    }

    thriftServices.getClient('UserService', '0', userService, (err, client) => {
      if (err) {
        return next(err)
      }

      if (!client) {
        return res.sendexpress.serviceUnavailable(null, 'UserService v0')
      }

      client.getContext(req.userId, (err, context) => {
        if (err) {
          return next(err)
        }

        req.user = context
        next()
      })
    })
  }
}
