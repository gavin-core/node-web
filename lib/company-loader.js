'use strict'

module.exports = () => {
  return (req, res, next) => {
    req.companyId = null
    const communityIdRegex = /^\/[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}/i

    const matches = communityIdRegex.exec(req.url)
    if (!matches || !matches.length) {
      return next()
    }

    req.companyId = matches[0].replace('/', '')
    next()
  }
}
