module.exports = function (options, confCb, onListeningCb) {
  if (typeof options === 'function') {
    onListeningCb = confCb
    confCb = options
    options = {}
  }

  const express = require('express')
  const http = require('http')
  const core = require('node-core')
  const portMe = require('port-me')
  const logger = core.logger
  const app = express()
  const _ = core._

  options.host = options.host || 'localhost'

  confCb = confCb || Object.__noop
  onListeningCb = onListeningCb || Object.__noop

  app.disable('x-powered-by')
  app.use(require('body-parser').json({ limit: '50mb', defer: !!options.defer }))

  if (options.defer) {
    app.use(options.defer)
  }

  app.use((req, res, next) => {
    req.data = _.extend({}, req.body, req.params, req.query)
    next()
  })

  confCb(app)

  const server = http.createServer(app)
  server.on('error', err => {
    logger.error('HTTP Server error:' + err.stack)
  })
  server.startOnPort = function (host, port, cb) {
    this.listen(port, host, cb)
  }

  if (!options.port && !options.portRange) {
    options.portRange = { min: 15000, max: 50000 }
  }

  if (!options.portRange) {
    return server.startOnPort(options.host, options.port, () => {
      onListeningCb(null, {
        protocol: 'http',
        host: options.host,
        port: options.port
      })
    })
  }

  portMe(options.portRange.min, options.portRange.max, (err, port) => {
    if (err) {
      return onListeningCb(new Error('Unable to find a port onwhich to start the http server'))
    }

    try {
      return server.startOnPort(options.host, port, () => {
        onListeningCb(null, {
          protocol: 'http',
          host: options.host,
          port: port
        })
      })
    } catch (e) {
      console.log(e)
      logger.verbose(`Error attempting to start http server on port: ${port}`)
    }
  })
}
